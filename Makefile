CC = gcc
CFLAGS = -Wall -pedantic -ansi
BIN = freq.exe
OBJ = freq.o frequencies.o transmitter.o coloring.c transmitter_stack.o 

%.o: %.c
	$(CC) -c $(CFLAGS) $< -o $@ -lm

$(BIN): $(OBJ)
	$(CC) $^ -o $@ -lm

