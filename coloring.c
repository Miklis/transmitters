/*____________________________________________________________________________
  Tento modul má implementované funkce zprostředovávající greedy algoritmus
  obarvování vrcholů grafu, kdy barvy představují frekvence
  ____________________________________________________________________________
 */
#include <stdlib.h>
#include <stdbool.h>
#include "frequencies.h"
#include "transmitter.h"
#include "transmitter_stack.h"

/*____________________________________________________________________________
  Výčtový typ pro určení stavu obarvování
  ____________________________________________________________________________
 */
typedef enum{
    NON_PROCESSED = 0,
    PROCESSED = 1,
    COLORED = 2
}coloring_state;


/*____________________________________________________________________________
  Funkce zjištuje, jestli je předaná frekvence volná, což znamená,
  že žádný z předaných sousedů do funkce nemá tuto frekvenci.

  Pokud je frekvence volná vrací true, pokud je již zabraná vrací false
  ____________________________________________________________________________
 */
bool is_frequency_free(neighbor *first, frequency *frequency){
    neighbor *tmp_neighbor = first;

    /* nemá sousedy */
    if(first == NULL){
        return true;
    }

    /* pokud je nejaka frekvence u kontrolovaného vysílace stejna tak vrati false */
    while(tmp_neighbor != NULL){
        if(tmp_neighbor->neighbor->trans->frequency != NULL){
            if(tmp_neighbor->neighbor->trans->frequency->id == frequency->id){
                return false;
            }
        }

        tmp_neighbor = tmp_neighbor->next;
    }

    /* frekvence je volna */
    return true;
}


/*____________________________________________________________________________
  Funkce přidává všechny sousední transmittery do zásobníku,
  pokud tam teda již nejsou, což se dá zjistit pomocí pomocného pole mark,
  kde je uložené informace, v jakém stavu je transmitter.

  Vrací return_code:
  OK (0) - vše proběhlo v pořádku
  FATAL_ERROR (2) - došlo k chybě s pamětí
  ____________________________________________________________________________
 */
return_code add_neighbours_to_stack(transmitter *transmitter ,int *mark, transmitters **stack){
    neighbor *tmp_neighbor = transmitter->neighbors;
    return_code result = OK;

    while(tmp_neighbor != NULL){
        if(mark[tmp_neighbor->neighbor->trans->id] == NON_PROCESSED){
            if(result = transmitter_stack_push(stack, tmp_neighbor->neighbor->trans)) break;
            mark[tmp_neighbor->neighbor->trans->id] = PROCESSED;
        }
        tmp_neighbor = tmp_neighbor->next;
    }

    return result;
}

/*____________________________________________________________________________
  Funkce obstarávající algoritmus procházení grafu do hloubky (DFS).

  Funguje, tak že je ze zásobníku vyjmut prvek (není-li zásobník prázdný)
  a naopak jsou do zásobníku přidáni všichni jeho sousedi pokud tam již nejsou.
  Následně se pro vytažený prvek ze zásobníku zkoumá, jaká frekvence může být
  aktuálnímu transmitteru přiřazena. Toto se provádí,
  dokud není zásobník prázdný (obarvili se všechny prvky komponenty grafu)
  nebo dokud není jasné, že algoritmus řešení nenalezl.

  Vrací return_code
  OK (0) - bylo nalezeno korektní řešení
  FATAL_ERROR (2) - chyba v paměti,
  NON_EXISTENT_SOLUTION (3) - nebylo nalezeno řešení.
  ____________________________________________________________________________
 */
return_code dfs_coloring(frequencies **first, int *mark, transmitters **stack){
    frequencies *tmp_freq = NULL;
    transmitter *current = NULL;
    int frequency_free;
    return_code result = OK;

    while(*stack){
        transmitter_stack_pop(stack, &current);

        /* Vloží všechny sousedy do zásobníku */
        if(result = add_neighbours_to_stack(current, mark, &(*stack))){
            return result;
        }

        /* Začínáme zkoumat frekvence od začátku, aby se zjistilo,
           která první frekvence je volná pro vysílač
        */
        tmp_freq = *first;
        frequency_free = false;

        /* zkoumání všech frequency než se nalezne první volná */
        while(tmp_freq){
            frequency_free = is_frequency_free(current->neighbors, tmp_freq->frequency);
            if(frequency_free == true){
                current->frequency = tmp_freq->frequency;
                mark[current->id] = COLORED;
                break;
            }
            tmp_freq = tmp_freq->next;
        }

        /* žádná frekvence volná není tudíž algoritmus nenalezl řešení */
        if(frequency_free == false){
            result = NON_EXISTENT_SOLUTION;
            break;
        }
    }

    return result;
}

/*____________________________________________________________________________
  Nastaví všechny hodnoty předaného pole na 0.
  ____________________________________________________________________________
 */
void clear_array(int array_len, int *array){
    int i;

    for(i = 0; i < array_len;i++){
        array[i] = NON_PROCESSED;
    }

}

/*____________________________________________________________________________
  Tato funkce má za úkol přiřadit frekvenci vysílačům předaným v parametru.
  Postupuje se tak, že se nejdříve vytvoří pomocné pole o
  dálce počtu vysílačů, kde je uložená informace o tom,
  jestli s vysílačem nebylo děláno nic, byl zpracovám, či mu již byla
  předána frekvence. Následně se pak prochází všechny vysílače, a pokud
  vysílači nebyla dána frekvence, tak se spustí funkce pro obarvování
  komponenty grafu, kdy se komponenta prochází pomocí DFS.

  Funkce vrací return_code:
  OK (0) - všechny vrcholy byly obarveny v pořádku,
  FATAL_ERROR (2) - chyba při práci s pamětí,
  NON_EXISTENT_SOLUTION (3) - algoritmus nenalezl řešení problému
  nebo řešení neexistuje.
  ____________________________________________________________________________
 */
return_code coloring_color_vertices(transmitters **first_transmitter, frequencies **first_frequency){
    transmitters *it_transmitter = *first_transmitter;
    transmitters *stack = NULL;
    return_code result = OK;
    int *mark = malloc(transmitter_get_transmitter_number() * sizeof(int));

    /* Když není spojová struktura transmitterů prázná a frequencies, tak proveď obarvování*/
    if (*first_transmitter && *first_frequency) {
            clear_array(transmitter_get_transmitter_number(), mark);

        while (it_transmitter) {
            //transmitter (vrchol grafu) nemá přiřazenou frekvenci (barvu)
            if (it_transmitter->trans->frequency == NULL) {
                mark[it_transmitter->trans->id] = PROCESSED;

                /* nastala chyba při přidávání prvku do zásobníku */
                if (result = transmitter_stack_push(&stack, it_transmitter->trans)) break;

                /* spuštění obarvování komponenty grafu pomocí procházení do hloubky */
                if (result = dfs_coloring(first_frequency, mark, &stack)) break;
            }

            it_transmitter = it_transmitter->next;
        }
    }

    free(mark);
    transmitter_stack_free_all(&stack);

    return result;
}

