/*____________________________________________________________________________
  Pro více  informací k implementací funcí se podívejte do coloring.c
  ____________________________________________________________________________
 */
#ifndef TRANSMITTERS_C_COLORING_H
#define TRANSMITTERS_C_COLORING_H

#include "transmitter.h"
#include "return_code.h"

/* ____________________________________________________________________________

   Prototypy funkcí

   ____________________________________________________________________________
 */
return_code coloring_color_vertices(transmitters **first_transmitter, frequencies **first_frequency);

#endif
