/*____________________________________________________________________________
  Hlavním modul programu. Má na starost načítání dat,
  řízení kličových funkcí programu a výpis řešení.
  ____________________________________________________________________________
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "transmitter.h"
#include "frequencies.h"
#include "coloring.h"
#include "return_code.h"


/*____________________________________________________________________________
  Velikost čtecího bufferu
  ____________________________________________________________________________
 */
#define BUFFER_SIZE 50

/*____________________________________________________________________________
  Definované chybové hlášky
  ____________________________________________________________________________
 */
#define ERR1 "ERR#1: Missing argument!\n"
#define ERR2 "ERR#2: Out of memory!\n"
#define ERR3 "ERR#3: Non-existent solution!\n"
#define ERR4 "ERR#4: Invalid file path!\n"
#define ERR5 "ERR#5: Invalid transmitter format!\n"
#define ERR6 "ERR#6: Invalid radius format!\n"
#define ERR7 "ERR#7: Invalid frequency format!\n"

/*____________________________________________________________________________
  Definované záčátky jednotlivých sekcí souboru
  ____________________________________________________________________________
 */
#define AVAILABLE_FREQUENCIES "Available frequencies:"
#define TRANSMITTER_RADIUS "Transmission radius:"
#define TRANSMITTERS "Transmitters:"

/*____________________________________________________________________________
  Výčtový typ pro určení z jaké sekce souboru se má aktuálně číst.
  ____________________________________________________________________________
 */
typedef enum{
    NON_RECOQNIZED_FILE_SECTION = 0,
    FREQUNCIES_FILE_SECTION = 1,
    RADIUS_FILE_SECTION = 2,
    TRANSMITTERS_FILE_SECTION = 3
}file_section;

/*____________________________________________________________________________
  Funkce určí v jaké sekci souboru se čtení aktuálně nachází.

  Vrací příslušnou sekci, ze které se bude číst.
  ____________________________________________________________________________
 */
file_section resolve_file_category(char *line){
    /* odstraní poslední znak z řetězce */
    line[strlen(line)-1] = 0;

    if(strcmp(line, AVAILABLE_FREQUENCIES) == 0){
        return FREQUNCIES_FILE_SECTION;
    } else if(strcmp(line, TRANSMITTER_RADIUS) == 0){
        return RADIUS_FILE_SECTION;
    } else if(strcmp(line, TRANSMITTERS) == 0){
        return TRANSMITTERS_FILE_SECTION;
    } else{
        return NON_RECOQNIZED_FILE_SECTION;
    }
}

/*____________________________________________________________________________
  Načítá frekvence ze souboru a ukládá je rovnou do spojové struktury
  frequencies.

  Vrací return_code:
  OK (0) - čtění proběhlo v pořádku,
  FATAL_ERROR (2) - chyba v paměti,
  INVALID_FREQUENCY_FORMAT (7) - chyba při čtení frekvencí
  ____________________________________________________________________________
 */
return_code read_frequencies(FILE *fp, frequencies **first ,char *buff){
    int id, freq, argument_num = 2;
    frequencies *last = NULL, *tmp_frequencies = NULL, *previous = NULL;
    /* Předpoklad chyby */
    return_code result = INVALID_FREQUENCY_FORMAT;

    fgets(buff, BUFFER_SIZE, fp);

    if(resolve_file_category(buff) == FREQUNCIES_FILE_SECTION){
        while(fgets(buff, BUFFER_SIZE, fp) != NULL && sscanf(buff, "%d %d", &id, &freq) == argument_num){
            result = frequencies_create_new(id, freq, &tmp_frequencies);

            /* Chyba při vytváření frequencies */
            if(result != OK) break;

            /* Vložení prvního do struktury */
            if(id == 0){
                *first = tmp_frequencies;
                last = *first;
            } else{
                /* vkládání dalších na konec */
                previous = last;
                last = tmp_frequencies;
                previous->next = last;
            }
        }
    }

    return result;
}

/*____________________________________________________________________________
  Čte ze souboru hodnotu radius

  Vrací return_code
  OK (0) - čtení proběhlo v pořádku,
  INVALID_RADIUS_FORMAT (6) - chyba při čtení radiusu
  ____________________________________________________________________________
 */
return_code read_radius(FILE *fp, char *buff){
    int radius;
    return_code result = INVALID_RADIUS_FORMAT;

    if(resolve_file_category(buff) == RADIUS_FILE_SECTION){
        fgets(buff, BUFFER_SIZE, fp);
        radius = atoi(buff);
        transmitter_set_transmitter_radius(radius);
        result = OK;
    }

    return result;
}

/*____________________________________________________________________________
  Čte ze souboru data k vysílačům a zároveň je ukládá do spojové struktury
  transmitters.

  Vrací return_code
  OK (0) -  čtění proběhlo v pořádku
  FATAL_ERROR (2) - chyba v paměti
 INVALID_TRANSMITTER_FORMAT (5) - chyba při čtení vysílačů
  ____________________________________________________________________________
 */
return_code read_transmitters(FILE *fp, transmitters **first ,char *buff){
    int id, number_of_transmitters = 0, argument_num = 3;
    double lon, lat;
    transmitters *last = NULL, *tmp_transmitter = NULL, *previous = NULL;
    return_code result = INVALID_TRANSMITTER_FORMAT;

    fgets(buff, BUFFER_SIZE, fp);

    if(resolve_file_category(buff) == TRANSMITTERS_FILE_SECTION) {
        while (fgets(buff, BUFFER_SIZE, fp) != NULL && sscanf(buff, "%d %lf %lf", &id, &lon, &lat) == argument_num) {
            ++number_of_transmitters;
            result = transmitter_create_new_transmitter(id, lon, lat, &tmp_transmitter);

            /* Chyba při vytváření transmitteru */
            if (result != OK){
                return result;
            }

            if (id == 0) {
                /* Vložení prvního prvku */
                *first = tmp_transmitter;
                last = *first;
            } else {
                /* Vložení ostatních prvků na konec struktury */
                previous = last;
                last = tmp_transmitter;
                previous->next = last;
            }
        }

        transmitter_set_transmitter_number(number_of_transmitters);
    }

    return result;
}


/*____________________________________________________________________________
  Funkce čte data ze soubory ke kterému je předána cesta (path v parametru)
  a načítaná data ukládá do struktur first_trans a first_freq.

  Vrací return_code:
  OK (0) čtení proběhlo v pořádku
  FATAL_ERROR (2) chyba v paměti
  INVALID_FREQUENCY_FORMAT (5) chyba při čtení transmitteru
  INVALID_RADIUS_FORMAT (6) chyba při čtení radiusu
  INVALID_FREQUENCY_FORMAT (7) chyba při čtení frequencies
  ____________________________________________________________________________
 */
return_code read_data(transmitters **first_trans, frequencies **first_freq , char *path){
    FILE *fp;
    char buff[BUFFER_SIZE];
    return_code result = INVALID_FILE_PATH;

    fp = fopen(path, "r");

    if(fp){
        result = read_frequencies(fp, first_freq,buff);

        /* jestli prošlo předchozí čtení ze souboru, tak proveď */
        if(result == OK){
            result = read_radius(fp, buff);
        }

        /*jestli prošlo předchozí čtení ze souboru, tak proveď */
        if(result == OK){
            result = read_transmitters(fp, first_trans,buff);
        }
    }

    fclose(fp);

    return result;
}

/*____________________________________________________________________________
  Funkce vypíše nápovědu pokud uživatel špatně zadá vstup.
  ____________________________________________________________________________
 */
void help(){
    printf("Usage: freq.exe <filename>\n\n");
    printf("<filename> - Path to file with input data.\n");
}

/*____________________________________________________________________________
  Funkce vypíše řešení řešené programem nebo příslušnou chybovou hlášku
  ____________________________________________________________________________
 */
void print_solution(return_code *result, transmitters *first_trans){
    switch (*result){
        case OK:
            transmitter_print_transmitters(first_trans);
            break;
        case MISSING_ARGUMENT:
            printf(ERR1);
            help();
            break;
        case FATAL_ERROR:
            printf(ERR2);
            break;
        case NON_EXISTENT_SOLUTION:
            printf(ERR3);
            break;
        case INVALID_FILE_PATH:
            printf(ERR4);
            break;
        case INVALID_TRANSMITTER_FORMAT:
            printf(ERR5);
            break;
        case INVALID_RADIUS_FORMAT:
            printf(ERR6);
            break;
        case INVALID_FREQUENCY_FORMAT:
            printf(ERR7);
            break;
    }
}


/*____________________________________________________________________________
  Hlavní funkce programu
  ____________________________________________________________________________
 */
int main(int argc, char **argv) {
    transmitters *first_trans = NULL;
    frequencies *first_freq  = NULL;
    return_code result = MISSING_ARGUMENT;

    if(argc == 2){
        result = read_data(&first_trans, &first_freq, argv[1]);
        if(result == OK){

            result = transmitter_build_graph(&first_trans);
            if(result == OK){
                result = coloring_color_vertices(&first_trans, &first_freq);
            }
        }
    }

    print_solution(&result, first_trans);

    transmitter_free_all(&first_trans);
    frequencies_free_all(&first_freq);

    return result;
}