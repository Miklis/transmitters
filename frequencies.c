/*____________________________________________________________________________
  Tento modul obsahuje funkce pro manipulaci s typem frequencies
  ____________________________________________________________________________
 */

#include <stdio.h>
#include <stdlib.h>
#include "frequencies.h"

/*____________________________________________________________________________
  Vytvoří nový prvek typu frequencies a vrátí ho
  přes ukazatel na ukazatel out_frequencies

  Funkce vrací return code:
  OK (0) - vše proběhlo v pořádku
  FATAL_ERROR (2) - chyba při alokaci paměti
  ____________________________________________________________________________
 */
return_code frequencies_create_new(int id, int freq, frequencies **out_frequencies ){
    frequency *tmp_frequency = malloc(sizeof(frequency));
    frequencies *tmp_frequencies = malloc(sizeof(frequencies));
    return_code result = FATAL_ERROR;

    if(tmp_frequency && tmp_frequencies){
        tmp_frequency->id = id;
        tmp_frequency->freq = freq;

        tmp_frequencies->next = NULL;
        tmp_frequencies->frequency = tmp_frequency;

        *out_frequencies = tmp_frequencies;

        result = OK;
    }

    return result;
}

/*____________________________________________________________________________
  Uvolní paměť všem prvkům typu frequencies
  Do funkce se vkládá spojová struktura, kdy je předán jako
  parametr ukazatel na první prvek.
  ____________________________________________________________________________
 */
void frequencies_free_all(frequencies **first){
    frequencies *to_clear = NULL;

    while (*first) {
        to_clear = *first;
        *first = (*first)->next;
        free(to_clear->frequency);
        free(to_clear);
    }

    *first = NULL;

}

/*____________________________________________________________________________
  Vypíše všechny frekvence, které jsou vloženy do funkce jako
  uzatel na první prvek spojové struktury
  ____________________________________________________________________________
 */
void frequencies_print_all(frequencies *first){
    frequencies *it = first;

    while (it){
        printf("ID[%d] and freq: %d \n", it->frequency->id, it->frequency->freq);
        it = it->next;
    }
}