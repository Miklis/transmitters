/*____________________________________________________________________________
  Pro více informací k implementací funcí se podívejte do frequencies.c
  ____________________________________________________________________________
 */
#ifndef TRANSMITTERS_FREQUENCIES_H
#define TRANSMITTERS_FREQUENCIES_H

#include "return_code.h"

/* ____________________________________________________________________________

    Struktury a datové typy

   ___________________________________________________________________________
*/
typedef struct thefrequency{
    int id;                         /* id frequency */
    int freq;                       /* frekvence v Hz */
}frequency;

typedef struct thefrequencies{
    frequency *frequency;           /* ukazatel na frequency */
    struct thefrequencies *next;    /* ukazatel na další prvek spojové struktury */
}frequencies;

/* ____________________________________________________________________________

   Prototypy funkcí

   ____________________________________________________________________________
 */
return_code frequencies_create_new(int id, int freq, frequencies **out_frequencies);

void frequencies_print_all(frequencies *first);

void frequencies_free_all(frequencies **first);
#endif
