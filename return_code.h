#ifndef TRANSMITTERS_C_RETURN_CODE_H
#define TRANSMITTERS_C_RETURN_CODE_H

/* ____________________________________________________________________________

    Struktury a datové typy

   ___________________________________________________________________________
*/
typedef enum {
    OK                          = 0,
    MISSING_ARGUMENT            = 1,
    FATAL_ERROR                 = 2,
    NON_EXISTENT_SOLUTION       = 3,
    INVALID_FILE_PATH           = 4,
    INVALID_TRANSMITTER_FORMAT  = 5,
    INVALID_RADIUS_FORMAT       = 6,
    INVALID_FREQUENCY_FORMAT    = 7
}return_code;

#endif
