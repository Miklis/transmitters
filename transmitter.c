/*____________________________________________________________________________
  Model pro oparece s datovým typem transmitter. Modul umí vytvářet nové prvky,
  dále má funkce pro vzájemné nalezení všech sousedních transmitterů
  na základě hodnoty radius, kdy se pomocí této operace vytvoří neorientovaný graf
  reprezentovaný spojovou strukturou.
 ____________________________________________________________________________
 */
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "transmitter.h"
#include "return_code.h"

/*____________________________________________________________________________
  Počet vytvořených transmiterů
  ____________________________________________________________________________
 */
static int transmitter_number;

/*____________________________________________________________________________
  Vysílací radius transmitteru
  ____________________________________________________________________________
 */
static int transmitter_radius;

/*____________________________________________________________________________
  Nastaví počet transmitterů
  ____________________________________________________________________________
 */
void transmitter_set_transmitter_number(int number){
    transmitter_number = number;
}

/*____________________________________________________________________________
  Vrátí počet transmiterů
  ____________________________________________________________________________
 */
int transmitter_get_transmitter_number(){
    return transmitter_number;
}

/*____________________________________________________________________________
  Nastaví v jakém radisu vysílají vysílače
  ____________________________________________________________________________
 */
void transmitter_set_transmitter_radius(int radius){
    transmitter_radius = radius;
}

/*____________________________________________________________________________
  Z předaných hodnot x a y vytvoří novou lokaci

  Funkce vrací return code:
  OK (0) - vše proběhlo v pořádku
  FATAL_ERROR (2) - chyba při alokaci paměti
  ____________________________________________________________________________
 */
return_code create_new_location(double x, double y, location **out_location){
    location *loc = malloc(sizeof(location));

    if(loc){
        loc->x = x;
        loc->y = y;
        *out_location = loc;
    }

     return OK;
}

/*____________________________________________________________________________
  Vytvoří jednotlivé vysílače s jejich pozicemi.

  Funkce vrací return code:
  OK (0) - vše proběhlo v pořádku
  FATAL_ERROR (2) - chyba při alokaci paměti
  ____________________________________________________________________________
 */
return_code transmitter_create_new_transmitter(int id, double x, double y, transmitters **out_transmitters){
    location *loc = NULL;
    transmitter *tmp_transmitter = malloc(sizeof(transmitter));
    transmitters *tmp_trasmitters = malloc(sizeof(transmitters));
    return_code result = FATAL_ERROR;

    if(tmp_transmitter && tmp_trasmitters){
        /* Selhání při vytváření lokace */
        if((result = create_new_location(x, y, &loc)) == OK){
            tmp_transmitter->location = loc;
            tmp_transmitter->id = id;
            tmp_transmitter->neighbors = NULL;
            tmp_transmitter->frequency = NULL;

            tmp_trasmitters->trans = tmp_transmitter;
            tmp_trasmitters->next = NULL;

            result = OK;
            *out_transmitters = tmp_trasmitters;
        }
    }

    return result;
}

/*____________________________________________________________________________
  Metoda počítá vzdálenost mezi dvěma vysílači,
  kdy na tyto vysílače jsou předány ukazatele v parametrech funkce.

  Vzdálenost se počítá ve 2D prostoru.

  Funkce vrací výsledek v podobě reálného čísla.
  ____________________________________________________________________________
 */
double calculate_distance_between_transmitter(transmitter *first, transmitter *second){
   double res, x1, x2, y1, y2;

   /* Ošetření případu, kdy by byl ukazatel first nebo second NULL */
    if(first && second){
       x1 = first->location->x;
       y1 = first->location->y;
       x2 = second->location->x;
       y2 = second->location->y;

       res = sqrt(((x2 - x1) * (x2 - x1)) + ((y2 - y1) * (y2 - y1)));
   }

   return res;
}

/*____________________________________________________________________________
  Funkce přidá souseda do spojové struktury všech sousedů
  transmitteru.

  Funkce vrací return code:
  OK (0) - vše proběhlo v pořádku
  FATAL_ERROR (2) - chyba při alokaci paměti
  ____________________________________________________________________________
 */
return_code add_neighbor(transmitters *start, transmitters *next){
    neighbor *nei = malloc(sizeof(neighbor));
    neighbor *tmp_neighbor = NULL;
    return_code result = FATAL_ERROR;

    if(nei){
        nei->neighbor = next;
        nei->next = NULL;
        /* přidání prvního sousedního vysílače */
        if(!start->trans->neighbors){
            start->trans->neighbors = nei;
        } else{
            tmp_neighbor = start->trans->neighbors;
            /* je potřeba se dostat na konec spojové struktury sousedících vrcholů */
            while (tmp_neighbor->next){
                tmp_neighbor = tmp_neighbor->next;
            }
            tmp_neighbor->next = nei;
        }
        result = OK;
    }

    return result;
}

/*____________________________________________________________________________
  Tato metoda má za úkol sestavit graf ze spojové struktury vysílačů,
  kdy je metodě předán ukazatel na ukazatel na první prvek.

  Funkce vrací return code:
  OK (0) - vše proběhlo v pořádku
  FATAL_ERROR (2) - chyba při alokaci paměti
  ____________________________________________________________________________
 */
return_code transmitter_build_graph(transmitters **first){
    transmitters *tmp1 = NULL;
    transmitters *tmp2 = NULL;
    double result_distance = 0.0;
    tmp1 = *first;

    if(*first){
        while(tmp1){
            tmp2 = *first;
            while(tmp2){
                if(tmp1 == tmp2){
                    tmp2 = tmp2->next;
                    continue;
                }else{
                    result_distance = calculate_distance_between_transmitter(tmp1->trans, tmp2->trans);
                    /* transmitter_radius * 2 - funkce result distance distance nám vrátí
                      vzdálenost mezi dvěma transmittery a proto je potřeba vynásobit
                      radius * 2, protože defakto nás zajímá vzdálenost mezi transmittery
                      a pomocí hodnoty radius zjistíme jestli spolu kolidují
                    */
                    if(transmitter_radius * 2 > result_distance){
                        add_neighbor(tmp1, tmp2);
                    }
                }
                tmp2 = tmp2->next;
            }
            tmp1 = tmp1->next;
        }
    }

    return OK;
}


/*____________________________________________________________________________
  Vypíše všechny transmittery obsažené v předávané spojové struktuře
  v parametru jako ukzatel na první prvek, kdy je pro každý transmitter
  vypsáno na jaké jiné transmittery vede z toho konrétního neorientovaná hrana
  na kolizní transmittery.
  ____________________________________________________________________________
 */
void transmitter_print_graph(transmitters *first){
    transmitters *trans_tmp = first;
    neighbor    *nei_tmp   = NULL;

    if(first){
        while(trans_tmp){
            nei_tmp = trans_tmp->trans->neighbors;
            printf("Vrchol s id[%d]: ", trans_tmp->trans->id);
            while (nei_tmp){
                printf("%d -> ", nei_tmp->neighbor->trans->id);
                nei_tmp = nei_tmp->next;
            }

            printf("\n");
            trans_tmp = trans_tmp->next;
        }
    }
}

/*____________________________________________________________________________
  Vypíše informace o transmitterech uložených ve spojové struktuře,
  která je předaná jako parametr ukazatel na první prvek.
  Vypisuje se ve tvaru <id-vysílače> <přiřazenená-frekvence-v-Hz>
  ____________________________________________________________________________
 */
void transmitter_print_transmitters(transmitters *first){
    transmitters *it = first;

    while(it){
        printf("%d %d\n", it->trans->id, it->trans->frequency->freq);
        it = it->next;
    }
}

/*____________________________________________________________________________
  Uvolní pamět spojové struktury sousedů vysílače.
  Přebírá si jako parametr ukazatel na ukazatel prvního souseda.
  ____________________________________________________________________________
 */
void transmitter_free_neighbors(neighbor **first){
    neighbor *to_clear = NULL;

    while (*first) {
        to_clear = *first;
        *first = (*first)->next;
        free(to_clear);
    }

    *first = NULL;

}

/*____________________________________________________________________________
  Uvolní pamět pro předaný transmitter v paremetru
  ____________________________________________________________________________
 */
void transmitter_free_transmitter(transmitter *trans){
    free(trans->location);
    transmitter_free_neighbors(&(trans)->neighbors);
}

/*____________________________________________________________________________
  Uvolní pamět spojové struktury transmitterů.
  Funkce si přebírá ukazaten na ukazatel prvního prvku
  ze spojové struktury.
  ____________________________________________________________________________
 */
void transmitter_free_all(transmitters **first) {
    transmitters *to_clear = NULL;

    while (*first) {
        to_clear = *first;
        *first = (*first)->next;
        transmitter_free_transmitter(to_clear->trans);
        free(to_clear->trans);
        free(to_clear);
    }

    (*first) = NULL;
}