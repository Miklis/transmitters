/*____________________________________________________________________________
  Pro více informací k implementací funcí se podívejte do transmitter.c
  ____________________________________________________________________________
 */
#ifndef TRANSMITTERS_TRANSMITTER_H
#define TRANSMITTERS_TRANSMITTER_H
#include "frequencies.h"
#include "return_code.h"
/* ____________________________________________________________________________

    Struktury a datové typy

   ___________________________________________________________________________
*/
typedef struct thelocation{
    double x;                           /* souřadnice x */
    double y;                           /* souřadnice y */
}location;

typedef struct theneighbor{
    struct thetransmitters *neighbor;   /* ukazatel na transmitter představující souseda */
    struct theneighbor *next;           /* ukazatel na další prvek ve spojové struktuře sousedů */
}neighbor;

typedef struct thetransmitter{
    int id;                             /* id transmitteru */
    location *location;                 /* umístění transmitteru */
    neighbor *neighbors;                /* sousedi jejiž radius se překrývá s dotyčným transmitterem */
    frequency *frequency;               /* frequency transmitteru */
}transmitter;

typedef struct thetransmitters{
    transmitter *trans;                 /* ukazatel na uložené transmitter */
    struct thetransmitters *next;       /* ukazatel na další prvek ve struktuře */
}transmitters;

/* ____________________________________________________________________________

   Prototypy funkcí

   ____________________________________________________________________________
 */
void transmitter_set_transmitter_number(int number);

int transmitter_get_transmitter_number();

void transmitter_set_transmitter_radius(int radius);

return_code transmitter_create_new_transmitter(int id, double x, double y, transmitters **out_transmitters);

return_code transmitter_build_graph(transmitters **first);

void transmitter_print_graph(transmitters *first);

void transmitter_print_transmitters(transmitters *first);

void transmitter_free_all(transmitters **first);

#endif
