/*____________________________________________________________________________
  Tento modul pracuje s abstraktní datovou strukturou zásobník,
  kdy obsahuje základní funkce vložení a vybrání prvku ze zásobníku.
  Zásobník pracuje pouze s typem transmitter.
  ____________________________________________________________________________
 */
#include <stdlib.h>
#include <stdbool.h>
#include "transmitter_stack.h"

/*____________________________________________________________________________
  Vloží prvek do zásobníku.

  Funkce vrací return code:
  OK (0) - vše proběhlo v pořádku
  FATAL_ERROR (2) - chyba při alokaci paměti
  ____________________________________________________________________________
 */
return_code transmitter_stack_push(transmitters **stack, transmitter *value){
    transmitters *stack_el = malloc(sizeof(transmitter));

    if(!(stack_el)){
        return FATAL_ERROR;
    }

    stack_el->trans = value;
    stack_el->next = NULL;

    if(!(*stack)){
        *stack = stack_el;
    } else{
        stack_el->next = *stack;
        *stack = stack_el;
    }

    return OK;
}

/*____________________________________________________________________________
  Funkce vyjme vrchní prvek ze zásobníku a vrátí ho
  přes parametr out_transmitter. Při vyjímání prvku se zároveň smaže prvek
  ze zásobníku a uvolní se pamět mazaného prvku.

  vrací true pokud je zásobník prázdný a false pokud ne.
  ____________________________________________________________________________
 */
bool transmitter_stack_pop(transmitters **stack, transmitter **out_transmitter){
    transmitters *to_free = NULL;

    /* Kontrola jestli není zásobní prázdný */
    if(!(*stack)){
        return true;
    }

    *out_transmitter = (*stack)->trans;

    to_free = *stack;
    *stack = (*stack)->next;

    free(to_free);
    return  false;
}

/*____________________________________________________________________________
  Uvolní veškerou paměť zásobníku, kdy je zásobník předán do funkce
  jako ukazatel na ukazatel
  ____________________________________________________________________________
 */
void transmitter_stack_free_all(transmitters **stack){
    transmitter *tmp = NULL;

    while(transmitter_stack_pop(stack, &tmp) != true);

    *stack = NULL;
}