/*____________________________________________________________________________
  Pro více  informací k implementací funcí se podívejte do stack.c
  ____________________________________________________________________________
 */
#ifndef TRANSMITTERS_C_TRANSMITTER_STACK_H
#define TRANSMITTERS_C_TRANSMITTER_STACK_H

#include "transmitter.h"
#include "return_code.h"

/* ____________________________________________________________________________

   Prototypy funkcí

   ____________________________________________________________________________
 */
return_code transmitter_stack_push(transmitters **stack, transmitter *value);

bool transmitter_stack_pop(transmitters **stack, transmitter **out_transmitter);

void transmitter_stack_free_all(transmitters **stack);

#endif
